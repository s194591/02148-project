package org.openjfx;

import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.openjfx.controller.ClientController;
import org.openjfx.controller.LobbyController;
import org.openjfx.controller.ServerController;
import org.openjfx.controller.GameController;
import org.openjfx.controller.TitleScreenController;
import org.openjfx.model.Model;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;

/**
 * JavaFX App
 */
public class App extends Application {

    private Scene scene;
    private Model model = new Model(this);
    public static Stage stage;
  

    static ImagePattern playerImage = new ImagePattern(new Image("org/openjfx/PlayerSprite-1.png"));
    public static ImagePattern boxImage = new ImagePattern(new Image("org/openjfx/BoxSprite-1.png"));
    public MediaPlayer mediaPlayer;
    public AudioClip shootSound;

    @Override
    public void start(Stage stage) throws IOException {
    	App.stage=stage;
        shootSound = new AudioClip(ClassLoader.getSystemResource("org/openjfx/pew-pew-lame-sound-effect.mp3").toString());
        shootSound.setVolume(0.25);
        

        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("titlescreenNew.fxml"));
        Parent root = fxmlLoader.load();
        ((TitleScreenController)fxmlLoader.getController()).setViewModel(this, model);
        scene = new Scene(root);
        scene.getStylesheets().add(getClass().getResource("styles.css").toExternalForm());
        stage.setScene(scene);
        stage.show();
        stage.setResizable(false);
        stage.setTitle("Worms... But better!");

        
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                System.exit(0);
            }
        });
    }

    public void setServerScene() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("serverScreen.fxml"));
        Parent root = fxmlLoader.load();
        ServerController serverController = fxmlLoader.getController();
        serverController.setViewModel(this, model);
        scene.setRoot(root);
    }

    public void setClientScene() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("clientScreen.fxml"));
        Parent root = fxmlLoader.load();
        ClientController cC = fxmlLoader.getController();
        cC.setViewModel(this, model);
        scene.setRoot(root);
    }

    public static void main(String[] args) {
        launch();
    }

    public void startLobby(String uri) throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("lobbyScreen.fxml"));
        Parent root = fxmlLoader.load();
        LobbyController lobbyController = fxmlLoader.getController();
        lobbyController.setViewModel(this, model);
        lobbyController.startServer(uri);
        scene.setRoot(root);

    }

    public void joinLobby(String uri, String name) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("lobbyScreen.fxml"));
        Parent root = fxmlLoader.load();
        LobbyController lobbyController = fxmlLoader.getController();
        lobbyController.setViewModel(this, model);
        lobbyController.joinLobby(uri, name);
        scene.setRoot(root);
    }
    
    public void startGame(String name) throws IOException{
    	FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("gameScene.fxml"));
    	Parent root = fxmlLoader.load();
        GameController gameController = fxmlLoader.getController();
    	gameController.setViewModel(this, model); 
    	root.setId("pane");
    	scene.setRoot(root);
    	App.stage.setHeight(540);
    	App.stage.setWidth(1455);
    	
    	scene.setOnKeyPressed(v -> model.playerInput.put(v.getCode(), true));
        scene.setOnKeyReleased(e -> model.playerInput.put(e.getCode(), false));
    	/*
    	scene.setOnKeyPressed( v -> {
            switch(v.getCode()) {
        	case A:
        		try {
					model.sendPlayerMovement("left",model.id);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		
        		break;
        	case W:
        		try {
					model.sendPlayerMovement("up",model.id);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		
        		break;
        	case D:
        		try {
					model.sendPlayerMovement("right",model.id);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				};
        		break;
           
        }});*/
    	
    }
   
    
    public static class playerModel extends Rectangle {
    	
    	private String type;
    	private boolean jumpEnabled= false;
    	public int health = 100;
    	
    	
    	public playerModel(double locX,double locY, int w,int h, String type,Color color){
    		super(w,h,color);
    		
    		this.type=type;
    		
    		setFill(playerImage);
    		
    		setTranslateX(locX);
    		setTranslateY(locY);
    		
    	}
    	
    	public boolean checkJump() {
    		return jumpEnabled;
    	}
    	
    	public void setJump(boolean val) {
    		this.jumpEnabled= val;
    	}
    	
    	public void moveLeft() {
    		System.out.println("old left");
    		setTranslateX(getTranslateX()-20);
    	}
    	
    	public void moveRight() {
    		System.out.println("old right");
    		setTranslateX(getTranslateX()+20);
    	}
    	
    	public void moveX(int amount) {
    		boolean Right = amount >0;
    		
    		 for (int i = 0; i < Math.abs(amount); i++) {
    			 for (Rectangle rec : Model.rectangles) {
    				 if (getBoundsInParent().intersects(rec.getBoundsInParent())) {
    					 if (Right) {
    	                        if (getTranslateX() + 40 == rec.getTranslateX()) {
    	                            return;
    	                        }
    	                    }
    	                    else {
    	                        if (getTranslateX() == rec.getTranslateX() + 60) {
    	                            return;
    	                        }
    	                    }
    	                }
    	            }
    	            setTranslateX(getTranslateX() + (Right ? 1 : -1));
    	        }
    		
    	}
    	
    	public void moveY(int amount) {
    		boolean Down = amount > 0;

            for (int i = 0; i < Math.abs(amount); i++) {
                for (Rectangle rec : Model.rectangles) {
                    if (getBoundsInParent().intersects(rec.getBoundsInParent())) {
                        if (Down) {
                            if (getTranslateY() + 40 == rec.getTranslateY()) {
                                setTranslateY(getTranslateY() - 1);
                                jumpEnabled = true;
                                return;
                            }
                        }
                        else {
                            if (getTranslateY() == rec.getTranslateY() + 60) {
                                return;
                            }
                        }
                    }
                }
               setTranslateY(getTranslateY() + (Down ? 1 : -1));
            }
    	}
    	
    }
    
    public static class bulletModel extends Circle {
    	public int id;
    	public Timeline timeline;

    	public bulletModel(int id,Point2D position) {
    		super(position.getX(), position.getY(),5, Color.YELLOW);
    		this.id=id;
    		
    	}
    }
}







package org.openjfx.model;
import java.util.HashMap;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.util.Duration;
import org.jspace.FormalField;
import org.jspace.RemoteSpace;
import org.jspace.SequentialSpace;
import org.jspace.Space;
import org.openjfx.App;
import org.openjfx.controller.GameController;
import org.openjfx.controller.LobbyController;
import javafx.geometry.Point2D;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

import java.io.IOException;
import java.util.ArrayList;

public class Model {

    private App app;
    private RemoteSpace clientChat;

    public static final String URI_PREFIX = "tcp://";
    public static final String URI_POSTFIX = ":9001/?keep";
    public static final String URI_CHAT = ":9001/chat?keep";
    public static final String URI_LOBBY = ":9001/lobby?keep";

    public static int numPlayers = 1;
    public App.playerModel[] players=  new App.playerModel[4];

    public GameController gameController;

    public String clientName = "Host";
    public int id = 0;

    public static ArrayList<Thread> threads = new ArrayList<>();
    public  Point2D[] playerVelocitys = new Point2D[4];
    public static ArrayList<Rectangle> rectangles = new ArrayList<>();
    public static ArrayList<App.bulletModel> bullets = new ArrayList<>();
    public static ArrayList<Point2D> dirVects = new ArrayList<>();
    private ArrayList<Space> spaces = new ArrayList<>();
    public HashMap<KeyCode,Boolean> playerInput = new HashMap<KeyCode, Boolean>();
    ////////////////////////////
    //// GETTERS & SETTERS /////
    ////////////////////////////

    public Model(App app) {
        this.app = app;
    }
    public ArrayList<Space> getSpaces() {
        return spaces;
    }
    public void setClientName(String name){
        clientName = name;
    }
    public String getClientName() {
        return clientName;
    }
    public void setClientChat(RemoteSpace clientChat) {
        this.clientChat = clientChat;
    }

    /////////////////////////////////
    //// COMMUNICATION FUNCTIONS ////
    /////////////////////////////////

    public void sendDataToClients(String function, String[] arguments) throws InterruptedException {
        for (Space space : spaces){
            space.put(function, arguments);
        }
    }

    public void sendDataToServer(String function, String[] args) throws InterruptedException {
        clientChat.put(function, args);
    }

    public void rpcServerHandle(LobbyController lobbyController, SequentialSpace chat) throws InterruptedException {

        Object[] resp = chat.get(new FormalField(String.class),
                new FormalField(String[].class));
        String[] args = (String[]) resp[1];

        switch ((String)resp[0]) {
            case "updateChat":
                lobbyController.updateChat(args[0], args[1]);
                break;
            case "shotData":
                spawnShot(Double.parseDouble(args[0]), Double.parseDouble(args[1]));
                break;
            case "playerMovement":
                movePlayer(Integer.parseInt(args[0]),(args[1]));
                break;
            case "updateConnectedPlayers":
                lobbyController.connectedPlayers.appendText("\n" + args[0]);
                sendDataToClients((String)resp[0], new String[]{args[0], lobbyController.connectedPlayers.getText()});
                return;
            case "bulletData":
                createBullet(new Point2D(Double.parseDouble(args[0]),
                                         Double.parseDouble(args[1])),
                             Integer.parseInt(args[2]));
                break;
            default:
                System.out.println("Unknown RPC " + resp[0]);
                return;
        }

        sendDataToClients((String)resp[0], args);

    }

    public void rpcClientHandle(LobbyController lobbyController, Space listenerSpace) throws InterruptedException, IOException {
        // RPC example: "updateChat", "name" , "message"
        Object[] resp = listenerSpace.get(new FormalField(String.class),
                new FormalField(String[].class));
        String[] args = (String[]) resp[1];

        switch ((String)resp[0]) {
            case "updateChat":
                lobbyController.updateChat(args[0], args[1]);
                break;
            case "startGame":
                id = Integer.parseInt(args[0]);
                numPlayers = Integer.parseInt(args[1]);
                createPlayers();
                app.startGame(clientName);
                break;
            case "updateConnectedPlayers":
                lobbyController.connectedPlayers.setText(args[1]);
                break;
            case "shotData":
                spawnShot(Double.parseDouble(args[0]), Double.parseDouble(args[1]));
                break;
            case "playerMovement":
                movePlayer(Integer.parseInt(args[0]), (args[1]));
                break;
            case "bulletData":
                createBullet(new Point2D(Double.parseDouble(args[0]),Double.parseDouble(args[1])),Integer.parseInt(args[2]));
                break;
            default:
                System.out.println("Unknown RPC : " + resp[0]);
        }
    }

    public void clientStartGame() throws InterruptedException {
        int playerID = 1;
        for (int i = 0; i < numPlayers-1; i++){
            spaces.get(i).put("startGame", new String[] {""+playerID++, numPlayers+""} );
        }
    }

    public void sendBulletData(Point2D targetCoords) throws InterruptedException {
        app.shootSound.play();
        if (id == 0){
            sendDataToClients("bulletData", new String[]{targetCoords.getX()+"",targetCoords.getY()+"",id+""});
            createBullet(targetCoords, id);
        } else {
            sendDataToServer("bulletData", new String[] {targetCoords.getX()+"",targetCoords.getY()+"",id+""
            });
        }
    }

    public void sendPlayerMovement(String keyPressed, int playerID) throws InterruptedException {
        if (id==0) {
            sendDataToClients("playerMovement", new String[]{playerID+"", keyPressed});
            movePlayer(playerID, keyPressed);
        }else {
            clientChat.put("playerMovement", new String[]{playerID+"", keyPressed });
        }
    }

    //////////////////////////////
    //// ANIMATION & GRAPHICS ////
    ////   RELATED FUNCTIONS  ////
    //////////////////////////////

    public void createGameContent(Pane gamePane) {

    	for (int i = 0; i < MapEditor.gameMap.length; i++) {
            String line = MapEditor.gameMap[i];
            for (int j = 0; j < line.length(); j++) {
                switch (line.charAt(j)) {
                    case '0':
                        break;
                    case '1':
                        Rectangle obj = createObjects(j*60, i*60, 60, 60, Color.BROWN,gamePane);
                        rectangles.add(obj);
                        break;
                    
                }
            }
        }
    	
    }
    
    public Rectangle createObjects(int locX,int locY, int w, int h, Color color, Pane gamePane) {
    	Rectangle obj = new Rectangle(w,h,color);
    	obj.setTranslateX(locX);
    	obj.setTranslateY(locY);
    	gamePane.getChildren().add(obj);
    	
    	obj.setFill(App.boxImage);
    	
    			
    	return obj;
    }
    
    
    public void update() {
    	
    	for (int i=0; i < numPlayers;i++) {
    		if ( playerVelocitys[i].getY() < 10) {
    		playerVelocitys[i] = playerVelocitys[i].add(0,1);
    	}
    	players[i].moveY((int)playerVelocitys[i].getY());

    	}

    	if (playerInput.getOrDefault(KeyCode.A,false) ) {
    		//System.out.println("deez");
    		try {
				sendPlayerMovement("left",id);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	
    	if (playerInput.getOrDefault(KeyCode.D,false) ) {
    		try {
				sendPlayerMovement("right",id);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			};
    	}
    	
    	if(playerInput.getOrDefault(KeyCode.W,false)) {
    		try {
				sendPlayerMovement("up",id);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}

    	bulletCollision();
    	
    }

    public void createPlayers() {
    	
    	for (int i=0; i < numPlayers; i++) {
    		players[i] = new App.playerModel(i*50,0,40,40,"deez", Color.RED);
    		playerVelocitys[i] = new Point2D(0,0);
    		
    	}
    }

    public void createBullet(Point2D targetCoords, int id) throws InterruptedException {
        calculateDirection(targetCoords, id);

        App.bulletModel bullet = new App.bulletModel(id, getPlayerCoords(id));
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                gameController.gamePane.getChildren().add(bullet);
                createBulletObj(bullet);
                //bullet.animate();
                animateBullet(bullet);
                //app.mediaPlayer.play();
            }
        });
    }

    public void createBulletObj(App.bulletModel bullet) {
        bullets.add(bullet);
    }

    public void animateBullet(App.bulletModel bullet) {

        int distance = 5000;
        int time = distance*2;

        Timeline timeline = new Timeline();

        int i = Model.bullets.indexOf(bullet);
        Point2D dir = Model.dirVects.get(i);

        KeyValue startX = new KeyValue(bullet.translateXProperty(), players[bullet.id].getX());
        KeyValue endX = new KeyValue(bullet.translateXProperty(), players[bullet.id].getX() + distance * dir.getX());

        KeyFrame startFX = new KeyFrame(Duration.ZERO, startX);
        KeyFrame endFX = new KeyFrame(Duration.millis(time), endX);

        KeyValue startY = new KeyValue(bullet.translateYProperty(), players[bullet.id].getY());
        KeyValue endY = new KeyValue(bullet.translateYProperty(), players[bullet.id].getY() + distance * dir.getY());

        KeyFrame startFY = new KeyFrame(Duration.ZERO, startY);
        KeyFrame endFY = new KeyFrame(Duration.millis(time), endY);

        timeline.getKeyFrames().addAll(startFX, endFX, startFY, endFY);

        timeline.setAutoReverse(false);
        timeline.setCycleCount(1);
        timeline.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Model.bullets.remove(bullet);
                Model.dirVects.remove(dir);
                gameController.gamePane.getChildren().remove(bullet);
            }
        });
        bullet.timeline = timeline;
        timeline.play();
    }

    public void spawnShot(double x, double y) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                gameController.gamePane.getChildren().add(new App.playerModel(x,y,50,50,"deez", Color.RED));
            }
        });
    }

    public void movePlayer(int id, String direction) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if (direction.equals("left")) {
                    if(players[id].getTranslateX() >= 4) {
                        //players[id].moveLeft();
                    	players[id].moveX(-5);
                    	
                    }
                } else if (direction.equals("right")) {
                    if (players[id].getTranslateX() + 50 <= MapEditor.gameMap[0].length()*60 -4)
                        //players[id].moveRight();
                    	players[id].moveX(5);
                    	
                }else if (direction.equals("up")) {
                    if (players[id].getTranslateY() >= 4 && players[id].checkJump()) {
                        playerVelocitys[id] = playerVelocitys[id].add(0,-30);
                        players[id].setJump(false);
                    }
                }

            }
        });
    }

    /////////////////////////////////
    //// MATHEMATICAL FUNCTIONS /////
    /////////////////////////////////

    public Point2D getPlayerCoords(int id) {
    	return new Point2D( players[id].getTranslateX(), players[id].getTranslateY() );
    }
    
    public void calculateDirection(Point2D targetCoords, int id) {
    	Point2D playerCoords = getPlayerCoords(id);
    	dirVects.add(targetCoords.subtract(playerCoords).normalize());
    }

    private void bulletCollision() {
        bulletloop:
        for (App.bulletModel bullet : bullets){
            for (Rectangle rectangle : rectangles) {
                if (bullet.getBoundsInParent().intersects(rectangle.getBoundsInParent())){
                    int ind = bullets.indexOf(bullet);
                    if (ind != -1) {
                        bullet.timeline.jumpTo(Duration.INDEFINITE);
                        continue bulletloop;
                    }
                }
            }
            for (int i = 0; i < players.length; i++){
                App.playerModel player;
                if (i == id || i == bullet.id) continue;
                try {
                    player = players[i];
                    if (bullet.getBoundsInParent().intersects(player.getBoundsInParent())){
                        bullet.timeline.jumpTo(Duration.INDEFINITE);
                        continue bulletloop;
                    }
                } catch (NullPointerException ignored) {
                }
            }
        }
    }
}

package org.openjfx.controller;

import javafx.fxml.FXML;
import org.openjfx.App;
import org.openjfx.model.Model;

import java.io.IOException;

public class TitleScreenController {

    private App app;
    private Model model;

    @FXML
    private void toServer() throws IOException {
        app.setServerScene();
    }

    @FXML
    private void toClient() throws IOException {
        app.setClientScene();
    }

    public void setViewModel(App app, Model model){
        this.app = app;
        this.model = model;
    }

}

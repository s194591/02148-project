package org.openjfx.controller;

import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;
import org.openjfx.App;
import org.openjfx.model.MapEditor;
import org.openjfx.model.Model;
import javafx.fxml.FXML;
import javafx.scene.layout.Pane;

public class GameController  {

	
	private App app;
    private Model model;

    @FXML
    public Pane gamePane;
    @FXML private Text healthBar1;
    @FXML private Text healthBar2;
    @FXML private Text healthBar3;
    @FXML private Text healthBar4;
    

	public void setViewModel(App app, Model model) {
        this.app = app;
        this.model = model;

        model.gameController = this;

        for (App.playerModel player : model.players ) {
            if (player == null) continue;
            gamePane.getChildren().add(player);
        }
        model.createGameContent(gamePane);

        AnimationTimer timer = new AnimationTimer() {
            private long lastUpdate;
			@Override
            public void handle(long now) {
                if (now - lastUpdate >= 28_000_000) {
                    lastUpdate = now ;
                    model.update();
                }
            }
    	}; 
    	timer.start();
    	
    	

        gamePane.setOnMouseClicked( e -> {
        	switch(e.getButton()) {
        	case PRIMARY:
        		Point2D targetCoords = new Point2D(e.getX() , e.getY());

                try {
                    model.sendBulletData(targetCoords);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }

                break;
            
        	/*case SECONDARY:
        		try {
        			//player.moveRight();
	                //this.model.sendShotData(e.getX(),e.getY());
                    model.animateBullets();
        		}catch (Exception ex) {
        			ex.printStackTrace();
                }
        		break;*/
            
        	}});

    }
	
	
    public void spawnShot(double x, double y) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                gamePane.getChildren().add(new App.playerModel(x,y,50,50,"deez", Color.RED));
            }
        });
    }
    
    public void movePlayer(int id, String direction) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
            	if (direction.equals("left")) {
            		if(model.players[id].getTranslateX() >= 4) {
            		model.players[id].moveLeft();
            		}
            	} else if (direction.equals("right")) {
            		if (model.players[id].getTranslateX() + 50 <= MapEditor.gameMap[0].length()*60 -4)
            		model.players[id].moveRight();
            	}else if (direction.equals("up")) {
            		if (model.players[id].getTranslateY() >= 4 && model.players[id].checkJump()) {
            			model.playerVelocitys[id] = model.playerVelocitys[id].add(0,-30);
                		model.players[id].setJump(false);
                		
            		}
            	}
            	
            }
        });
    }
}

package org.openjfx.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import org.jspace.*;
import org.openjfx.App;
import org.openjfx.listeners.ServerHandler;
import org.openjfx.listeners.ClientHandler;
import org.openjfx.model.Model;
import java.io.IOException;
import java.util.ArrayList;

public class LobbyController {

    @FXML private Button startButton;
    @FXML private TextArea chat;
    @FXML public TextArea connectedPlayers;
    @FXML private TextArea metaInf;
    @FXML private TextArea messageInfo;

    public App app;
    public Model model;

    private boolean SERVER_HOST = false;

    @FXML
    private void startGame() throws IOException, InterruptedException {
    	model.createPlayers();
    	app.startGame(model.getClientName());
    	model.clientStartGame();
    	startButton.setVisible(false);
    }

    @FXML
    private void send() throws InterruptedException {
        if (SERVER_HOST) { // Host sending message
            chat.appendText("Host: " + messageInfo.getText() + "\n");
            model.sendDataToClients("updateChat", new String[]{model.clientName, messageInfo.getText()});
        } else { // Client sending message
            model.sendDataToServer("updateChat", new String[]{model.clientName, messageInfo.getText()});
        }
        messageInfo.clear();
    }
    

    public void setViewModel(App app, Model model) {
        this.app = app;
        this.model = model;
    }
    
    public void updateConnectedPlayers(String name) throws InterruptedException {
        model.sendDataToServer("updateConnectedPlayers", new String[] {name, ""});
    }

    public void startServer(String ip) {
        SERVER_HOST = true;
        metaInf.setText("Join at: " + ip);
        connectedPlayers.appendText("\nHost");
        Thread t = new Thread(new ServerHandler(ip, this));
        t.start();
        Model.threads.add(t);
    }

    public void joinLobby(String ip, String name) {
        System.out.println("Joining lobby");
        startButton.setVisible(false);

        Thread t = new Thread(new ClientHandler(ip, this, name));
        t.start();
        Model.threads.add(t);
    }

    public ArrayList<Space> getSpaces() {
        return model.getSpaces();
    }

    public void setClientChat(RemoteSpace clientChat, String clientName) {
        model.setClientName(clientName);
        model.setClientChat(clientChat);
    }

    public void updateChat(String name, String text) {
        chat.appendText(name+": " + text + "\n");
    }
}


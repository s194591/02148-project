package org.openjfx.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import org.openjfx.App;
import org.openjfx.model.Model;

import java.io.IOException;

public class ClientController {

    @FXML private TextArea uriField;
    @FXML private TextArea nameField;

    private App app;
    private Model model;

    @FXML
    private void connect() throws IOException {
    	if (uriField.getText().isEmpty()){     	// default value of local address for quick debugging
    		 app.joinLobby("127.0.0.1", nameField.getText());
    	}else {
    		 app.joinLobby(uriField.getText(), nameField.getText());
        }
    	
       
    }

    public void setViewModel(App app, Model model){
        this.app = app;
        this.model = model;
    }

}

package org.openjfx.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

import org.openjfx.App;
import org.openjfx.model.Model;

import java.io.IOException;

public class ServerController {

    public static String URI;
    private App app;
    private Model model;

    @FXML private TextArea uriField;

    @FXML
    private void startServer() throws IOException {
    	if (uriField.getText().isEmpty()){    	// default value of local address for quick debugging
    		URI="127.0.0.1";
    	}else {
        URI = uriField.getText();
        }
        app.startLobby(URI);
    }

    public void setViewModel(App app, Model model){
        this.app = app;
        this.model = model;
    }

}
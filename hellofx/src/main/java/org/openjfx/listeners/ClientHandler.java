package org.openjfx.listeners;

import org.jspace.ActualField;
import org.jspace.FormalField;
import org.jspace.RemoteSpace;
import org.jspace.Space;
import org.openjfx.controller.LobbyController;
import org.openjfx.model.Model;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class ClientHandler implements Runnable {

    private String IP;
    private LobbyController lobbyController;
    private String clientName;

    public ClientHandler(String IP, LobbyController lobbyController, String name) {
        this.IP = IP;
        this.lobbyController = lobbyController;
        this.clientName = name;
    }

    @Override
    public void run() {
        try {

            System.out.println("Connecting to " + IP);
            RemoteSpace lobby = new RemoteSpace(Model.URI_PREFIX + IP + Model.URI_LOBBY);

            lobby.put(clientName);
            Object[] objects = lobby.get(new ActualField(clientName), new FormalField(String.class), new FormalField(Integer.class));

            if (((String)objects[1]).contentEquals("ACCESS_DENIED")){
                System.out.println("Lobby full");
                lobbyController.app.setClientScene();
                return;
            }

            RemoteSpace chat = new RemoteSpace((String)objects[1]);
            lobbyController.setClientChat(chat, clientName);

            URI uri = new URI(Model.URI_PREFIX + IP + Model.URI_POSTFIX);

            System.out.println("Connecting to : tcp://"+uri.getHost()+":9001/" + clientName+objects[2] + "?keep");
            RemoteSpace listenerSpace = new RemoteSpace("tcp://"+uri.getHost()+":9001/"+clientName+objects[2]+"?keep");


            Thread t = new Thread(new ChatListener(listenerSpace, lobbyController));
            t.start();
            Model.threads.add(t);

            lobbyController.updateConnectedPlayers(clientName);

        } catch (IOException e){
            System.out.println("CLIENT HANDLER ERROR");
            try {
                lobbyController.app.setClientScene();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } catch (URISyntaxException | InterruptedException e) {
            System.out.println("CLIENT HANDLER ERROR");
            e.printStackTrace();
        }
    }
}

class ChatListener implements Runnable {

    private Space listenerSpace;
    private LobbyController lobbyController;

    public ChatListener(Space listenerSpace, LobbyController lobbyController){
        this.listenerSpace = listenerSpace;
        this.lobbyController = lobbyController;
    }

    @Override
    public void run() {
        try {
            while (true){
                lobbyController.model.rpcClientHandle(lobbyController, listenerSpace);
            }
        } catch (Exception e){
            System.out.println("CLIENT LISTENER ERROR");
            e.printStackTrace();
        }
    }
}
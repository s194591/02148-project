package org.openjfx.listeners;

import org.jspace.FormalField;
import org.jspace.SequentialSpace;
import org.jspace.Space;
import org.jspace.SpaceRepository;
import org.openjfx.controller.LobbyController;
import org.openjfx.model.Model;

import java.util.ArrayList;

public class ServerHandler implements Runnable {

    private String IP;
    private LobbyController lobbyController;

    public ServerHandler(String IP, LobbyController lc){
        this.IP = IP;
        this.lobbyController = lc;
    }

    @Override
    public void run() {
        try {
            SpaceRepository repo = new SpaceRepository();

            SequentialSpace lobby = new SequentialSpace();
            SequentialSpace chat = new SequentialSpace();

            repo.add("chat", chat);
            repo.add("lobby", lobby);

            // Open a gate
            String gateUri = Model.URI_PREFIX + IP + Model.URI_POSTFIX ;
            System.out.println("Opening repository gate at " + gateUri);

            repo.addGate(gateUri);

            Thread t = new Thread(new LobbyHandler(lobbyController.getSpaces(), lobby, repo, IP));
            t.start();
            Model.threads.add(t);

            System.out.println("Listening for new chats");
            while (true){
                lobbyController.model.rpcServerHandle(lobbyController, chat);
            }

        } catch (Exception e){
            System.out.println("SERVER HANDLER ERROR");
            e.printStackTrace();
        }
    }
}

class LobbyHandler implements Runnable {

    private ArrayList<Space> spaces;
    private SequentialSpace lobby;
    private SpaceRepository spaceRepository;
    private String IP;

    private int id = 0;

    public LobbyHandler(ArrayList<Space> spaces, SequentialSpace sequentialSpace, SpaceRepository spaceRepository, String IP) {
        this.spaces = spaces;
        this.lobby = sequentialSpace;
        this.spaceRepository = spaceRepository;
        this.IP = IP;
    }

    @Override
    public void run() {
        try {
            System.out.println("Listener for clients to join: ");

            while (true){
                String name = (String)lobby.get(new FormalField(String.class))[0];

                if (Model.numPlayers >= 4){
                    lobby.put(name, "ACCESS_DENIED", -1);
                    continue;
                }

                System.out.println("Found chatter : " + name);

                SequentialSpace space = new SequentialSpace();
                spaces.add(space);
                spaceRepository.add(name+id, space);

                String gateUri = Model.URI_PREFIX + IP + Model.URI_CHAT;

                lobby.put(name, gateUri, id++);
                Model.numPlayers++;
            }
        } catch (Exception e){
            System.out.println("LOBBY HANDLER ERROR");
            e.printStackTrace();
        }
    }
}
